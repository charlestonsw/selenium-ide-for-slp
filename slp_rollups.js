/**
 * For use in Selenium IDE.
 *
 /**
 * For use in Selenium IDE.
 *
 * You will need to add this to Selenium under Options / Options in the Selenium Menu.
 *
 * You will also want to Go With The Flow scripts.
 * https://bitbucket.org/lance_cleveland/go_with_the_flow
 *
 * Put this under Selenium Core Extensions:
 * <install-dir>/go_with_the_flow.js , <install-dir>slp_rollups.js
 */
var manager = manager || new RollupManager();

/**
 * check_for_syntax_errors
 *
 * This rollup tests for php syntax errors, warnings, and notices.
 */
manager.addRollupRule({
    name: 'check_for_syntax_errors',
    description: 'Check for PHP syntax errors, notices, warnings.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'setSpeed'                 , target: '0'                                         , value: ''                                 });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*Notice: Undefined*'              });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '**Notice: Trying to get*'         });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*Notice: Use of*'                 });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*Fatal error:*'                   });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*Strict Standards:*'              });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*Warning: call_user_func_array*'  });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: '*failed to open stream*'          });
        commands.push({ command: 'assertNotText'            , target: '//body'                                    , value: 'headers already sent'             });
        commands.push({ command: 'assertElementNotPresent'  , target: "//p[@class='wpdberror']"                   , value: ''                                 });
        commands.push({ command: 'assertElementNotPresent'  , target: "//div[contains(@class,'is-error active')]" , value: ''                                 });
        commands.push({ command: 'setSpeed'                 , target: '200'                                       , value: ''                                 });

        return commands;
    }
});

/**
 * Open tab
 */
manager.addRollupRule({
    name: 'open_tab',
    description: 'Open the settings tab and check for errors.',
    args: [ { name: 'tab' , description: 'tab name' } ],
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'setSpeed'             , target: '200'                                     , value: ''                         });
        commands.push({ command: 'open'                 , target: '/wp-admin/admin.php?page=' + args.tab    , value: ''                         });
        commands.push({ command: 'waitForElementPresent', target: 'id=footer-thankyou'                      , value: ''                         });
        commands.push({ command: 'rollup'               , target: 'check_for_syntax_errors'                 , value: ''                         });
        commands.push({ command: 'setSpeed'             , target: '0'                                       , value: ''                         });

        return commands;
    }
});

/**
 * Open Locations Page
 */
manager.addRollupRule({
    name: 'open_locations_page',
    description: 'Open the locations page and check for errors.',
    args: [ { name: 'params' , description: 'additional parameters' } ],
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'setSpeed'             , target: '400'                         , value: ''                         });
        commands.push({ command: 'open'                 , target: '/locations' + args.params    , value: ''                         });
        commands.push({ command: 'waitForElementPresent', target: 'id=colophon'                 , value: ''                         });
        commands.push({ command: 'rollup'               , target: 'check_for_syntax_errors'     , value: ''                         });
        commands.push({ command: 'setSpeed'             , target: '0'                           , value: ''                         });

        return commands;
    }
});

/**
 * Open Locations Page
 */
manager.addRollupRule({
    name: 'open_ui_page',
    description: 'Open any UI page and check for errors.',
    args: [ { name: 'params' , description: 'additional parameters' } ],
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'setSpeed'             , target: '200'                         , value: ''                         });
        commands.push({ command: 'open'                 , target: '/' + args.params             , value: ''                         });
        commands.push({ command: 'waitForElementPresent', target: 'id=colophon'                 , value: ''                         });
        commands.push({ command: 'rollup'               , target: 'check_for_syntax_errors'     , value: ''                         });
        commands.push({ command: 'setSpeed'             , target: '0'                           , value: ''                         });

        return commands;
    }
});

/**
 * Check Premier Subscription
 */
manager.addRollupRule({
    name: 'check_premier_subscription',
    description: 'Check the subscription is valid , if not ask for the sub id.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];
        commands.push({ command: 'assertEval'           , target: 'storedVars[\'premier_active\']'          , value: 'true'                 });
        commands.push({ command: 'rollup'               , target: 'open_tab'                                , value: 'tab=slp_general'      });
        commands.push({ command: 'storeElementPresent'  , target: '//div[@id=\'wpcsl_settings_group-add_on_packs\']\/\/p[contains(.,\'Premier Subscription has been validated\')]' , value: 'subscription_valid' });
        commands.push({ command: 'ifFalse'              , target: 'storedVars[\'subscription_valid\']'     , value: '' });
        commands.push({ command: 'rollup'               , target: 'add_premier_subscription'                , value: '' });

        return commands;
    }
});

/**
 * Add Premier Subscription Data
 */
manager.addRollupRule({
    name: 'add_premier_subscription',
    description: 'Add a premier subscription ID if needed.  For the ltesting user # 100.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'type'                 , target: 'id=options_nojs[premium_user_id]'        , value: '100'                  });
        commands.push({ command: 'storeEval'            , target: 'prompt("Subscription ID","40898_19900")' , value: 'subscription_id'      });
        commands.push({ command: 'type'                 , target: 'id=options_nojs[premium_subscription_id]', value: '${subscription_id}'   });
        commands.push({ command: 'rollup'               , target: 'save_settings'                           , value: ''                     });
        commands.push({ command: 'rollup'               , target: 'open_tab'                                , value: 'tab=slp_general'      });
        commands.push({ command: 'storeElementPresent'  , target: '//div[@id=\'wpcsl_settings_group-add_on_packs\']\/\/p[contains(.,\'Premier Subscription has been validated\')]' , value: 'subscription_valid' });

        return commands;
    }
});

/**
 * Set Active Plugins
 */
manager.addRollupRule({
    name: 'set_active_plugins',
    description: 'Set the active plugin vars.',
    args: [
        { name: 'plugins' , description: 'plugins to set separated with a space.' , exampleValues: ['experience', 'power', 'premier','experience power', 'experience premier power'] },
        { name: 'read_again' , description: 'Force us to re-read the plugin status.' },
    ],
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var plugins = args.plugins.split(' ');
        var num_plugins = plugins.length;
        var plugin_slug='';
        var plugin_row_selector='';

        args.read_again = ( typeof args.read_again === 'undefined' ) || args.read_again;
        var all_set = ( ! args.read_again );

        if ( all_set ) {
            for (var i = 0; i < num_plugins; ++i) {
                if (typeof storedVars[plugins[i] + '_active' ] === 'undefined') {
                    all_set = false;
                    break;
                }
            }
        }

        var commands = [];

        if ( ! all_set ) {
            commands.push({command: 'setSpeed', target: '200', value: 'tab=slp_experience'});
            commands.push({command: 'open', target: '/wp-admin/plugins.php', value: ''});
            commands.push({
                command: 'waitForElementPresent',
                target: 'css=div.tablenav.bottom > div.tablenav-pages.one-page > span.displaying-num',
                value: ''
            });
            commands.push({command: 'rollup', target: 'check_for_syntax_errors', value: ''});
            commands.push({command: 'setSpeed', target: '0', value: ''});

            // Loop through and set <slug>_active and <slug>_inactive variables
            for (var i = 0; i < num_plugins; ++i) {
                plugin_slug = 'store-locator-plus-' + plugins[i];
                plugin_row_selector = '//tr[@data-slug=\'' + plugin_slug + '\']';
                commands.push({
                    command: 'storeElementPresent',
                    target: plugin_row_selector + '//span[@class=\'activate\']//a[contains(text(),\'Activate\')]',
                    value: plugins[i] + '_inactive'
                });
                commands.push({
                    command: 'storeElementPresent',
                    target: plugin_row_selector + '//span[@class=\'deactivate\']//a[contains(text(),\'Deactivate\')]',
                    value: plugins[i] + '_active'
                });
                commands.push({command: 'store', target: plugin_row_selector, value: plugins[i] + '_plugin_row'});
            }
        } else {
            LOG.info( '--- Active Plugins already set.' );
        }

        for (var i = 0; i < num_plugins; ++i) {
            commands.push({command: 'echo', target: '--- ' + plugins[i] + '_active: ${' +  plugins[i] + '_active}', value: '' });
            commands.push({command: 'echo', target: '--- ' + plugins[i] + '_inactive: ${' +  plugins[i] + '_inactive}', value: '' });
        }


        return commands;
    }
});

/**
 * Check Initial Map Location Count
 **/
manager.addRollupRule({
    name: 'check_initial_location_count',
    description: 'Check the map loading location count.  Assumes infinite radius and no special settings like always show featured.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'rollup'               , target: 'open_tab'                            , value: 'tab=slp_experience'       });
        commands.push({ command: 'storeValue'           , target: 'options_nojs[initial_results_returned]'   , value: 'initial_results_returned' });
        commands.push({ command: 'rollup'               , target: 'open_tab'                            , value: 'tab=slp_manage_locations' });
        commands.push({ command: 'storeText'            , target: 'total_locations_top'                 , value: 'total_locations'          });
        commands.push({ command: 'storeEval'            , target: 'Math.min(${total_locations},${initial_results_returned})'  , value: 'initial_count'          });
        commands.push({ command: 'rollup'               , target: 'open_locations_page'                 , value: 'params='                  });
        commands.push({ command: 'verifyXpathCount'     , target: '//div[@class=\'results_wrapper\']'   , value: '${initial_count}'         });

        return commands;
    }
});

/**
 * Check Initial Map Location Count
 **/
manager.addRollupRule({
    name: 'save_settings',
    description: 'Save settings, wait for thankyou and check for syntax errors.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'waitForElementPresent', target: 'css=input.button-primary'            , value: ''       });
        commands.push({ command: 'clickAndWait'         , target: 'css=input.button-primary'            , value: ''       });
        commands.push({ command: 'waitForElementPresent', target: 'id=footer-thankyou'                  , value: ''       });
        commands.push({ command: 'rollup'               , target: 'check_for_syntax_errors'             , value: ''       });

        return commands;
    }
});

/**
 * Set Location ID for location by name
 **/
manager.addRollupRule({
    name: 'set_location_id',
    description: 'Set the location ID for the named location.',
    args: [ { name: 'name' , description: 'the location name' } ],
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'storeValue', target: "//tr[@name='" + args.name + "']/@data-id"           , value: 'location_id'       });
        commands.push({ command: 'echo', target: "${location_id}"           , value: ''       });

        return commands;
    }
});

/**
 * Set Location ID for location by name
 **/
manager.addRollupRule({
    name: 'save_location',
    description: 'Save a location from the 4.8+ edit form.',
    commandMatchers: [],
    getExpandedCommands: function(args) {
        var commands = [];

        commands.push({ command: 'clickAndWait'         , target: "css=#slp_form_buttons > input.button-primary" , value: ''       });
        commands.push({ command: 'waitForElementPresent', target: "id=footer-thankyou"                              , value: ''       });
        commands.push({ command: 'rollup'               , target: "check_for_syntax_errors"                         , value: ''       });
        commands.push({ command: 'setSpeed'                 , target: '0'                                         , value: ''                                 });

        return commands;
    }
});