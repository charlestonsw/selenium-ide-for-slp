var webdriver = require('selenium-webdriver');

var config = require( '../config/config' );

// Setup Logging
//
var log = webdriver.logging.getLogger('webdriver.http');
webdriver.logging.installConsoleHandler();
log.setLevel( webdriver.logging.Level.INFO );

// Setup the Web Driver
//
var driver = new webdriver.Builder().forBrowser( config.preferred_browser ).build();
driver.manage().window().setSize( config.window.width , config.window.height );
driver.manage().window().setPosition( config.window.top_position , config.window.left_position );

// Expose This
//
module.exports = {
    config: config,
    driver: driver,
    log: log,

    // Selenium Aliases
    By: webdriver.By,
    until: webdriver.until,

    // Common Tests

    /**
     * Wait up to 6 seconds for an input with matching a matching ID to show up.
     * Then types something into it.
     *
     * @param string id     the ID for an input
     * @param string value  what to type in the field
     */
    when_id_exists_type : function( id, value ) {
        driver.wait( webdriver.until.elementLocated( webdriver.By.id( id ) ) , config.setting.standard_wait )
            .then( function() {
                var el = driver.findElement( webdriver.By.id( id ) );
                el.click();
                el.clear();
                el.sendKeys( value );
            });
    },

    /**
     * Wait up to 6 seconds for an input with matching a matching ID to show up.
     * Then clicks it.
     *
     * @param string id     the ID for an input
     */
    when_id_exists_click : function( id ) {
        driver.wait( webdriver.until.elementLocated( webdriver.By.id( id ) ) , config.setting.standard_wait )
            .then( driver.findElement( webdriver.By.id( id ) ).click() );
    },

    /**
     * Wait up to 6 seconds for an input with matching a matching NAME to be visible.
     * Then clicks it.
     *
     * @param string name     the name for an input
     */
    when_name_visible_click : function( name ) {
        var by = webdriver.By.name( name );
        driver.wait( webdriver.until.elementLocated( by ) , config.setting.standard_wait );
        var el = driver.findElement( by );
        driver.wait( webdriver.until.elementIsVisible( el ) , config.setting.standard_wait );
        el.click();
    }
};