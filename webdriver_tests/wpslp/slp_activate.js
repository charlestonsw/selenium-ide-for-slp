var test = require( '../lib/common' );

test.log.info( 'Enable SLP' );

test.driver.get( test.config.site.url )
    .then( test.when_id_exists_click( 'language-continue' ) );

test.when_id_exists_type( 'weblog_title' , test.config.site.title  );
test.when_id_exists_type( 'user_login' , test.config.admin.user );
test.when_id_exists_type( 'pass1-text' , test.config.admin.pwd ) ;
test.when_name_visible_click( 'pw_weak') ;
test.when_id_exists_type( 'admin_email' , test.config.admin.email ) ;
test.when_id_exists_click( 'submit' ) ;

test.driver.sleep( test.config.setting.standard_wait ) ;