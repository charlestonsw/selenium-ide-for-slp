var test = require( '../lib/common' );

test.log.info( 'Login' );

test.driver.get( test.config.env.url + '/wp-login.php' )
    .then( test.when_id_exists_type( 'user_login' , test.config.env.user ) )
    .then( test.when_id_exists_type( 'user_pass' , test.config.env.pwd ) )
    .then( test.when_id_exists_click( 'rememberme' ) )
    .then( test.when_id_exists_click( 'wp-submit' ) )
    .then( test.driver.sleep( test.config.setting.standard_wait ) )
;