var test = require( '../lib/common' );

test.log.info( 'Activate Site' );

test.driver.get( test.config.env.url )
    .then( test.when_id_exists_click( 'language-continue' ) )
    .then( test.when_id_exists_type( 'weblog_title' , test.config.env.title ) )
    .then( test.when_id_exists_type( 'user_login' , test.config.env.user ) )
    .then( test.when_id_exists_type( 'pass1-text' , test.config.env.pwd ) )
    .then( test.when_name_visible_click( 'pw_weak') )
    .then( test.when_id_exists_type( 'admin_email' , test.config.env.email ) )
    .then( test.when_id_exists_click( 'submit' ) )
    .then( test.driver.sleep( test.config.setting.standard_wait ) )
;