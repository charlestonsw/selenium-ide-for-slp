/**
 * Set the environment to staging or production or development (default)
 *
 * Rather than edit this code you can set the environment variable NODE_ENV:
 *
 * $ NODE_ENV=production node login.js
 * $ NODE_ENV=staging node login.js
 * $ NODE_ENV=development node login.js
 */
var environment = process.env.NODE_ENV|| 'development';


//-----------------
// Things you probably want to change.
//-----------------

var browser = 'safari';

var window = {
    width: 1200,
    height: 800,
    top_position: 100,
    left_position: 100,
};



//-----------------
// Things you may want to change.
//-----------------


/**
 * General app settings.
 */
var setting = {
    short_wait: 2000,
    standard_wait: 6000,
    long_wait: 10000,
};




//-----------------
// Don't change this unless you know what you're doing.
//-----------------

/**
 * Load the confidential user stuff from ../env/<environment>-real.js
 */
var env = require( '../env/' + environment + '-real' );

/**
 * Export these things.
 */
module.exports = {
    env: env,
    preferred_browser: browser,
    setting: setting,
    window: window,
};